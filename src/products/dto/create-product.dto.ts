import { IsNotEmpty, MinLength, Min, IsPositive } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(8)
  name: string;
  @Min(1)
  @IsPositive()
  price: number;
}
